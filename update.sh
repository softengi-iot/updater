#!/usr/bin/env bash

echo "update: $(date "+%T")" >> update.log
echo "deb [trusted=yes] http://beiot.tk:7070/ beiot main" >> /etc/apt/sources.list
apt update
apt install beiot
echo "#!/bin/bash" >> /etc/cron.hourly/beiotupdater
echo "" >> /etc/cron.hourly/beiotupdater
echo "apt update" >> /etc/cron.hourly/beiotupdater
echo "apt install beiot" >> /etc/cron.hourly/beiotupdater
chmod +x /etc/cron.hourly/beiotupdater
